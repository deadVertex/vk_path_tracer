#!/bin/sh

# Controls
BUILD_ASSET_LOADER=0
BUILD_SHADERS=0

# TODO: Compiler flags
CXX_FLAGS="-O2 -g -mavx2 -DPLATFORM_LINUX -Ithirdparty"

CC=${CC:-clang}
CXX=${CXX:-clang++}

mkdir -p build/shaders

# Build shaders..
if [ $BUILD_SHADERS -ne 0 ]; then 
    glslangValidator src/shaders/presentation.vert.glsl -V -o build/shaders/presentation.vert.spv
    glslangValidator src/shaders/presentation.frag.glsl -V -o build/shaders/presentation.frag.spv
fi

# Build asset loader library
if [ $BUILD_ASSET_LOADER -ne 0 ]; then
  # Build miniz library
  #$CC $CFLAGS -O2 -o build/miniz.o -I external/tinyexr -c external/tinyexr/miniz.c

  # Build asset loader library
  #$CXX $CXX_FLAGS -O2 -o build/asset_loader.o -I src -I external/tinyexr/ -DTINYEXR_USE_THREAD=1 -c src/asset_loader/asset_loader.cpp
  $CXX $CXX_FLAGS -o build/asset_loader.o -I src -c src/asset_loader/asset_loader.cpp
fi

GLFW_LIBS=$(pkg-config --libs glfw3)
VULKAN_LIBS=$(pkg-config --libs vulkan)
$CXX $CXX_FLAGS -o build/main src/main.cpp build/asset_loader.o $GLFW_LIBS $VULKAN_LIBS
