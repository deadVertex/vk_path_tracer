#pragma once

//! Stores all of the resources which are associated with a VkImage object
struct ImageState
{
    u32 id;
    VkImage handle;
    VkDeviceMemory memory;
    VkImageView view;
    u32 width;
    u32 height;
    u32 layerCount;
    u32 layout;
};

//! Stores all of the vulkan resources which are bound to a VkSwapchainKHR
//! object
struct SwapchainState
{
    VkSwapchainKHR handle;
    VkImage images[MAX_SWAPCHAIN_IMAGES];
    VkImageView imageViews[MAX_SWAPCHAIN_IMAGES];
    ImageState depthImages[MAX_SWAPCHAIN_IMAGES];
    VkFramebuffer framebuffers[MAX_SWAPCHAIN_IMAGES];
    u32 imageWidth;
    u32 imageHeight;
    u32 imageCount;
    VkFormat imageFormat;
    VkRenderPass renderPass;
};

//! Stores all of the vulkan resources and id required for a RenderPass
struct RenderPassState
{
    u32 id;
    VkRenderPass handle;
    ImageState color;
    ImageState depth;
    VkFramebuffer framebuffer;
    u32 textureId;
    u32 width;
    u32 height;
    VkFormat format;
};

//! Stores all of the resources which are associated with a VkBuffer object
struct BufferState
{
    VkBuffer handle;
    VkDeviceMemory memory;
    void *data;
    u64 capacity;
};
