#pragma once

struct Texture
{
    vec3 *pixels;
    u32 width;
    u32 height;
};

extern b32 LoadTexture(const char *path, Texture *texture);
