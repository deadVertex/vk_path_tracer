#include <cstdio>
#include <cmath>
#include <cfloat>

#include "platform.h"
#include "math_utils.h"
#include "math_lib.h"
#include "asset_loader.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

b32 LoadTexture(const char *path, Texture *texture)
{
    b32 result = false;
    i32 x, y, n;
    f32 *pixels = stbi_loadf(path, &x, &y, &n, 3);

    // NOTE: This will do the inverse sRGB conversion for LDR images
    if (pixels != NULL)
    {
        texture->width = (u32)x;
        texture->height = (u32)y;
        texture->pixels = (vec3 *)malloc(sizeof(vec3) * x * y);
        memcpy(texture->pixels, pixels, sizeof(vec3) * x * y);
        stbi_image_free(pixels);
        result = true;
    }

    return result;
}
