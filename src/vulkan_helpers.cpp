ImageState CreateImageState(VkDevice device, VkPhysicalDevice physicalDevice,
    u32 width, u32 height, VkFormat format, VkImageUsageFlags usage,
    VkImageAspectFlags aspectFlags)
{
    ImageState image = {};
    image.width = width;
    image.height = height;
    image.layerCount = 1;
    image.handle = CreateImage(device, width, height, format, usage);
    image.memory =
        AllocateAndBindMemoryForImage(device, image.handle, physicalDevice);
    image.view = CreateImageView(device, image.handle, format, aspectFlags);
    image.layout = VK_IMAGE_LAYOUT_UNDEFINED;

    return image;
}

void DestroyImageState(VkDevice device, ImageState image)
{
    vkDestroyImageView(device, image.view, NULL);
    vkDestroyImage(device, image.handle, NULL);
    vkFreeMemory(device, image.memory, NULL);
}

SwapchainState ConfigureSwapchain(VkDevice device, VkSurfaceKHR surface,
    VkPhysicalDevice physicalDevice, VkRenderPass renderPass, u32 imageCount,
    VkFormat imageFormat, u32 imageWidth, u32 imageHeight)
{
    SwapchainState state = {};
    state.imageWidth = imageWidth;
    state.imageHeight = imageHeight;
    state.imageCount = imageCount;
    state.imageFormat = imageFormat;
    state.renderPass = renderPass;

    VkSwapchainKHR swapchain = CreateSwapchain(device, surface, imageCount,
        imageFormat, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR, imageWidth, imageHeight,
        VK_PRESENT_MODE_FIFO_KHR);
    state.handle = swapchain;

    // Check that we got as many images for our swapchain as we requested
    // because the swapchain allocates the images for us to use which depending
    // on the present mode might be more or less than what we requested.
    u32 availableImageCount = 0;
    VK_CHECK(
        vkGetSwapchainImagesKHR(device, swapchain, &availableImageCount, NULL));
    Assert(availableImageCount == imageCount);

    // Retrieve the images allocated by the swapchain for the color buffer
    VK_CHECK(
        vkGetSwapchainImagesKHR(device, swapchain, &imageCount, state.images));

    // Create the framebuffer for each image in our swapchain using the images
    // that were allocated for us when we created the swapchain
    for (u32 imageIndex = 0; imageIndex < imageCount; imageIndex++)
    {
        // A frame buffer requires an image view as an attachment which defines
        // how the framebuffer will write to the memory bound to the image.
        state.imageViews[imageIndex] = CreateImageView(device,
            state.images[imageIndex], imageFormat, VK_IMAGE_ASPECT_COLOR_BIT);

        state.depthImages[imageIndex] = CreateImageState(device, physicalDevice,
            imageWidth, imageHeight, VK_FORMAT_D32_SFLOAT,
            VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
            VK_IMAGE_ASPECT_DEPTH_BIT);

        VkImageView attachments[] = {
            state.imageViews[imageIndex], state.depthImages[imageIndex].view};

        state.framebuffers[imageIndex] = CreateFramebuffer(device, renderPass,
            attachments, ArrayCount(attachments), imageWidth, imageHeight);
    }

    return state;
}

void UnconfigureSwapchain(SwapchainState swapchainState, VkDevice device)
{
    // Release framebuffers and image views
    for (u32 i = 0; i < swapchainState.imageCount; i++)
    {
        vkDestroyFramebuffer(device, swapchainState.framebuffers[i], NULL);
        vkDestroyImageView(device, swapchainState.imageViews[i], NULL);
        DestroyImageState(device, swapchainState.depthImages[i]);
    }
    vkDestroySwapchainKHR(device, swapchainState.handle, NULL);
}

BufferState CreateBuffer(VkDevice device, VkPhysicalDevice physicalDevice,
    u32 size, VkBufferUsageFlags usage)
{
    VkMemoryPropertyFlags memoryProperties =
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

    // Create the VkBuffer which will store the meta data
    VkBufferCreateInfo createInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
    createInfo.size = size;
    createInfo.usage = usage;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VkBuffer buffer = VK_NULL_HANDLE;
    VK_CHECK(vkCreateBuffer(device, &createInfo, NULL, &buffer));

    // Choose which memory heap we will allocate this buffer from
    VkMemoryRequirements memoryRequirements = {};
    vkGetBufferMemoryRequirements(device, buffer, &memoryRequirements);

    u32 memoryTypeIndex = FindMemoryType(
        physicalDevice, memoryRequirements.memoryTypeBits, memoryProperties);

    // Allocate the memory
    VkMemoryAllocateInfo allocInfo = {VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO};
    allocInfo.allocationSize = memoryRequirements.size;
    allocInfo.memoryTypeIndex = memoryTypeIndex;

    VkDeviceMemory bufferMemory = VK_NULL_HANDLE;
    VK_CHECK(vkAllocateMemory(device, &allocInfo, NULL, &bufferMemory));

    // Bind the memory to our buffer
    vkBindBufferMemory(device, buffer, bufferMemory, 0);

    // ASSUMPTION: all buffers use host-visible memory for now so we can map it
    Assert(memoryProperties & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    void *data = NULL;
    VK_CHECK(vkMapMemory(device, bufferMemory, 0, VK_WHOLE_SIZE, 0, &data));

    BufferState result = {};
    result.handle = buffer;
    result.memory = bufferMemory;
    result.data = data;
    result.capacity = size;

    return result;
}
