inline f32 RayIntersectSphere(
    vec3 center, f32 radius, vec3 rayOrigin, vec3 rayDirection)
{
    vec3 m = rayOrigin - center; // Use sphere center as origin

    f32 a = Dot(rayDirection, rayDirection);
    f32 b = 2.0f * Dot(m, rayDirection);
    f32 c = Dot(m, m) - radius * radius;

    f32 d = b * b - 4.0f * a * c; // Discriminant

    f32 t = -1.0f;
    if (d > 0.0f)
    {
        f32 denom = 2.0f * a;
        f32 t0 = (-b - Sqrt(d)) / denom;
        f32 t1 = (-b + Sqrt(d)) / denom;

        t = t0; // Pick the entry point
    }

    return t;
}

internal RayIntersectTriangleResult RayIntersectTriangleMT(vec3 rayOrigin,
    vec3 rayDirection, vec3 a, vec3 b, vec3 c, f32 tmin = F32_MAX)
{
    RayIntersectTriangleResult result = {};
    result.t = -1.0f;

    vec3 T = rayOrigin - a;
    vec3 e1 = b - a;
    vec3 e2 = c - a;

    vec3 p = Cross(rayDirection, e2);
    vec3 q = Cross(T, e1);
    vec3 n = Cross(e1, e2);

    vec3 m = Vec3(Dot(q, e2), Dot(p, T), Dot(q, rayDirection));

    f32 det = 1.0f / Dot(p, e1);

    f32 t = det * m.x;
    f32 u = det * m.y;
    f32 v = det * m.z;
    f32 w = 1.0f - u - v;

    f32 f = Dot(rayDirection, n);

    if (u >= 0.0f && u <= 1.0f && v >= 0.0f && v <= 1.0f && w >= 0.0f &&
        w <= 1.0f && f < 0.0f)
    {
        result.t = t;
        result.normal = n;
        result.uv = Vec2(u, v);
    }

    return result;
}

inline simd_f32 Simd_F32(f32 x)
{
    simd_f32 result = _mm256_set1_ps(x);
    return result;
}

inline simd_f32 Dot(simd_vec3 a, simd_vec3 b)
{
    simd_f32 x = _mm256_mul_ps(a.x, b.x);
    simd_f32 y = _mm256_mul_ps(a.y, b.y);
    simd_f32 z = _mm256_mul_ps(a.z, b.z);

    simd_f32 result = _mm256_add_ps(x, y);
    result = _mm256_add_ps(result, z);

    return result;
}

inline simd_vec3 Simd_Vec3(vec3 v[8])
{
    simd_vec3 result;
    result.x = _mm256_set_ps(
        v[7].x, v[6].x, v[5].x, v[4].x, v[3].x, v[2].x, v[1].x, v[0].x);
    result.y = _mm256_set_ps(
        v[7].y, v[6].y, v[5].y, v[4].y, v[3].y, v[2].y, v[1].y, v[0].y);
    result.z = _mm256_set_ps(
        v[7].z, v[6].z, v[5].z, v[4].z, v[3].z, v[2].z, v[1].z, v[0].z);

    return result;
}

inline simd_f32 RayIntersectSphereSimd(simd_vec3 center, simd_f32 radius,
    simd_vec3 rayOrigin, simd_vec3 rayDirection)
{
    simd_f32 t = Simd_F32(-1.0f);

    // vec3 m = rayOrigin - rayDirection
    simd_vec3 m;
    m.x = _mm256_sub_ps(rayOrigin.x, center.x);
    m.y = _mm256_sub_ps(rayOrigin.y, center.y);
    m.z = _mm256_sub_ps(rayOrigin.z, center.z);

    // f32 a = Dot(rayDirection, rayDirection)
    simd_f32 a = Dot(rayDirection, rayDirection);

    // f32 b = 2.0f * Dot(m, rayDirection)
    simd_f32 b = _mm256_mul_ps(Simd_F32(2.0f), Dot(m, rayDirection));

    // f32 c = Dot(m, m) - radius * radius
    simd_f32 radiusSq = _mm256_mul_ps(radius, radius);
    simd_f32 c = Dot(m, m) - radiusSq;

    // f32 d = b * b - 4.0f * a * c
    simd_f32 bSq = _mm256_mul_ps(b, b);
    simd_f32 rightEq = _mm256_mul_ps(Simd_F32(4.0f), _mm256_mul_ps(a, c));
    simd_f32 d = _mm256_sub_ps(bSq, rightEq);

    // f32 denom = 2.0f * a
    simd_f32 denom = _mm256_mul_ps(Simd_F32(2.0f), a);

    // TODO: Save a few cycles by multiplying by reciprocal denom
    // f32 t0 = (-b - Sqrt(d)) / denom
    simd_f32 bNeg = _mm256_sub_ps(_mm256_setzero_ps(), b);
    simd_f32 dSqrt = _mm256_sqrt_ps(d);
    simd_f32 numerator = _mm256_sub_ps(bNeg, dSqrt);
    simd_f32 t0 = _mm256_div_ps(numerator, denom);

    // if (d > 0.0f)
    simd_f32 mask = _mm256_cmp_ps(d, _mm256_setzero_ps(), _CMP_GT_OQ);

    // WARNING: This might not be safe...
    _mm256_maskstore_ps((f32 *)&t, mask, t0);

    return t;
}

internal RayIntersectTriangleResult RayIntersectMesh(
    vec3 rayOrigin, vec3 rayDirection, Mesh mesh)
{
    RayIntersectTriangleResult meshResult = {};
    meshResult.t = -1.0f;

    u32 triangleCount = mesh.indexCount / 3;

    for (u32 triangleIndex = 0; triangleIndex < triangleCount; triangleIndex++)
    {
        u32 indices[3];
        indices[0] = mesh.indices[triangleIndex * 3 + 0];
        indices[1] = mesh.indices[triangleIndex * 3 + 1];
        indices[2] = mesh.indices[triangleIndex * 3 + 2];

        vec3 vertices[3];
        vertices[0] = mesh.vertices[indices[0]];
        vertices[1] = mesh.vertices[indices[1]];
        vertices[2] = mesh.vertices[indices[2]];

        RayIntersectTriangleResult triangleResult =
            RayIntersectTriangleMT(rayOrigin, rayDirection, vertices[0],
                vertices[1], vertices[2], F32_MAX);

        if (triangleResult.t > 0.0f)
        {
            if (triangleResult.t < meshResult.t || meshResult.t < 0.0f)
            {
                meshResult = triangleResult;
            }
        }
    }

    return meshResult;
}

internal RayPacketIntersectSceneResult RayIntersectScene(
    Scene *scene, vec3 *rayOrigins, vec3 *rayDirections)
{
    RayPacketIntersectSceneResult packetResult = {};
    for (u32 i = 0; i < 8; i++)
    {
        packetResult.entries[i].t = -1.0f;
    }

    for (u32 i = 0; i < 8; i++)
    {
        for (u32 meshIndex = 0; meshIndex < scene->meshCount; meshIndex++)
        {
            Mesh mesh = scene->meshes[meshIndex];
            RayIntersectTriangleResult meshResult =
                RayIntersectMesh(rayOrigins[i], rayDirections[i], mesh);

            RayIntersectSceneResult *result = &packetResult.entries[i];
            if (meshResult.t > 0.0f)
            {
                if (meshResult.t < result->t || result->t < 0.0f)
                {
                    result->t = meshResult.t;
                    result->materialId = 1;
                    result->normal = meshResult.normal;
                    result->textureCoordinates = meshResult.uv;
                    result->position =
                        rayOrigins[i] + rayDirections[i] * meshResult.t;
                }
            }
        }
    }

#if 0
    // Intersect 8 rays against 1 sphere
    for (u32 i = 0; i < scene->sphereCount; i++)
    {
        Sphere sphere = scene->spheres[i];

        simd_vec3 center;
        center.x = _mm256_set1_ps(sphere.center.x);
        center.y = _mm256_set1_ps(sphere.center.y);
        center.z = _mm256_set1_ps(sphere.center.z);

        simd_f32 radius = _mm256_set1_ps(sphere.radius);
        simd_vec3 simdRayOrigins = Simd_Vec3(rayOrigins);
        simd_vec3 simdRayDirections = Simd_Vec3(rayDirections);

        simd_f32 simdSphereT = RayIntersectSphereSimd(
            center, radius, simdRayOrigins, simdRayDirections);

        f32 sphereTValues[8];
        _mm256_storeu_ps(sphereTValues, simdSphereT);

        for (u32 j = 0; j < 8; j++)
        {
            f32 sphereT = sphereTValues[j];

            RayIntersectSceneResult *result = &packetResult.entries[j];
            vec3 rayOrigin = rayOrigins[j];
            vec3 rayDirection = rayDirections[j];

            if (sphereT > 0.0f)
            {
                if (sphereT < result->t || result->t < 0.0f)
                {
                    vec3 p = rayOrigin + rayDirection * sphereT;
                    result->t = sphereT;
                    result->materialId = sphere.materialId;
                    result->normal = Normalize(p - sphere.center);
                    vec2 sphereCoords =
                        ToSphericalCoordinates(p - sphere.center);
                    sphereCoords.x = sphereCoords.x / PI;
                    sphereCoords.y = 0.5f * (sphereCoords.y / PI) + 0.5f;
                    result->textureCoordinates = sphereCoords;
                    result->position = rayOrigin + rayDirection * sphereT;
                }
            }
        }
    }
#endif

    return packetResult;
}

inline vec3 RandomInUnitSphere(RandomNumberGenerator *rng)
{
    vec3 p = Vec3(0);
    while (1)
    {
        p = Vec3(RandomUnilateral(rng), RandomUnilateral(rng),
                RandomUnilateral(rng)) *
                2.0f -
            Vec3(1);

        if (Dot(p, p) >= 1.0)
        {
            break;
        }
    }

    return p;
}

inline vec3 SampleTextureNearest(Texture texture, vec2 v)
{
    f32 fx = v.x * texture.width;
    f32 fy = v.y * texture.height;

    u32 x = (u32)Floor(fx);
    u32 y = (u32)Floor(fy);

    Assert(x <= texture.width);
    Assert(y <= texture.height);

    vec3 sample = *(((vec3 *)texture.pixels) + (y * texture.width + x));

    vec3 result = sample;
    return result;
}

internal vec3 ComputeRadianceForLightPath(
    MaterialSystem *materialSystem, LightPathVertex *lightPath, u32 length)
{
    Texture texture = materialSystem->textureSystem.texture;
    vec3 radiance = Vec3(0);
    if (length > 0)
    {
        for (i32 i = length - 1; i >= 0; i--)
        {
            LightPathVertex vertex = lightPath[i];

            Assert(vertex.materialId < materialSystem->count);
            Material material = materialSystem->materials[vertex.materialId];

            vec2 textureCoordinates =
                vertex.textureCoordinates * 0.5f + Vec2(0.5f);
            vec3 albedo = Hadamard(material.albedo,
                SampleTextureNearest(texture, textureCoordinates));

            radiance =
                material.emission + Hadamard(albedo, radiance) *
                                        Dot(vertex.normal, vertex.incomingDir);
        }
    }

    return radiance;
}

internal vec3 CalculatePixelPositionOnFilm(
    vec2 pixelPosition, vec3 cameraP, u32 imageWidth, u32 imageHeight)
{
    f32 fx = pixelPosition.x / (f32)imageWidth;
    f32 fy = pixelPosition.y / (f32)imageHeight;

    // Generate ray
    f32 filmWidth = 1.0f;
    f32 filmHeight = 1.0f;
    if (imageWidth > imageHeight)
    {
        filmHeight = (f32)imageHeight / (f32)imageWidth;
    }
    else
    {
        filmWidth = (f32)imageWidth / (f32)imageHeight;
    }

    f32 halfFilmWidth = 0.5f * filmWidth;
    f32 halfFilmHeight = 0.5f * filmHeight;

    f32 horizontal = 2.0f * fx - 1.0f;
    f32 vertical = 2.0f * fy - 1.0f;

    vec3 forward = Vec3(0, 0, -1);
    vec3 right = Vec3(1, 0, 0);
    vec3 up = Vec3(0, 1, 0);
    f32 filmDist = 1.0f;

    vec3 filmCenter = cameraP + forward * filmDist;
    vec3 filmP = filmCenter + halfFilmWidth * horizontal * right +
                 halfFilmHeight * vertical * up;

    return filmP;
}

internal void PathTraceTile(
    Scene *scene, RandomNumberGenerator *rng, WorkingImage image, Tile tile)
{
    f32 halfPixelWidth = 0.5f / (f32)image.width;
    f32 halfPixelHeight = 0.5f / (f32)image.height;

    u32 sampleCount = SAMPLE_COUNT;

    u32 currentPixelX = tile.minX;
    u32 currentPixelY = tile.minY;

    u32 samplesRemainingForPixel = sampleCount;

    // TODO: Could easily have a bug where we terminate early with not
    // enough samples on the last pixel
    while (currentPixelX < tile.maxX && currentPixelY < tile.maxY)
    {
        // TODO: Avoid zeroing whole warp
        Warp warp = {};

        // Build primary rays
        for (u32 warpIndex = 0; warpIndex < 8; warpIndex++)
        {
            u32 x = currentPixelX;
            u32 y = currentPixelY;

            // Offset to sample from center
            vec2 pixelPosition = Vec2((f32)x, (f32)y) + Vec2(0.5);

            // Jitter pixel position for each sample
            pixelPosition += Vec2(halfPixelWidth * RandomBilateral(rng),
                halfPixelHeight * RandomBilateral(rng));

            vec3 cameraP = Vec3(0, 0, 5);
            vec3 filmP = CalculatePixelPositionOnFilm(
                pixelPosition, cameraP, image.width, image.height);

            warp.rayOrigin[warpIndex] = cameraP;
            warp.rayDirection[warpIndex] = Normalize(filmP - cameraP);
            warp.pixelX[warpIndex] = x;
            warp.pixelY[warpIndex] = y;

            // Initialize warp
            warp.pathLength[warpIndex] = 0;
            warp.radiance[warpIndex] = Vec3(0);

            samplesRemainingForPixel--;
            if (samplesRemainingForPixel == 0)
            {
                // Move on to next pixel when we run out of samples
                if (currentPixelX + 1 < tile.maxX)
                {
                    currentPixelX++;
                }
                else
                {
                    currentPixelX = tile.minX;
                    currentPixelY++;
                }
                // FIXME: This is overwriting our buffer! Luckily vulkan
                // doesn't seem to mind? Assert(currentPixelIndex <
                // totalPixelCount);
                samplesRemainingForPixel = sampleCount;
            }
        }

        // Compute light path for each primary ray (need to remember
        // which pixel each light path is for)
        u32 maxBounces = 3;
        for (u32 bounce = 0; bounce < maxBounces; bounce++)
        {
            // Wide
            RayPacketIntersectSceneResult packetRayIntersections =
                RayIntersectScene(scene, warp.rayOrigin, warp.rayDirection);

            for (u32 warpIndex = 0; warpIndex < 8; warpIndex++)
            {
                LightPathVertex *vertex =
                    warp.path[warpIndex] + warp.pathLength[warpIndex]++;
                vertex->outgoingDir = warp.rayDirection[warpIndex];

                RayIntersectSceneResult rayIntersection =
                    packetRayIntersections.entries[warpIndex];

                if (rayIntersection.t >= 0.0f)
                {
                    // Ray hit
                    vec3 target = rayIntersection.position +
                                  rayIntersection.normal +
                                  RandomInUnitSphere(rng);
                    vertex->incomingDir =
                        Normalize(target - rayIntersection.position);
                    vertex->normal = rayIntersection.normal;
                    vertex->textureCoordinates =
                        rayIntersection.textureCoordinates;
                    vertex->materialId = rayIntersection.materialId;

                    warp.rayOrigin[warpIndex] =
                        warp.rayOrigin[warpIndex] +
                        warp.rayDirection[warpIndex] * rayIntersection.t +
                        rayIntersection.normal * 0.0001f;
                    warp.rayDirection[warpIndex] = vertex->incomingDir;
                }
                else
                {
                    // Ray miss
                    vertex->materialId = 0;
                    warp.rayOrigin[warpIndex] = Vec3(0);
                    warp.rayDirection[warpIndex] = Vec3(0);
                }
            }
        }

        // Compute and accumulate radiance for each light path and write
        // to working image
        for (u32 warpIndex = 0; warpIndex < 8; warpIndex++)
        {
            vec3 radiance = ComputeRadianceForLightPath(&scene->materialSystem,
                warp.path[warpIndex], warp.pathLength[warpIndex]);

            u32 x = warp.pixelX[warpIndex];
            u32 y = warp.pixelY[warpIndex];
            if (x < image.width && y < image.height)
            {
                image.pixels[y * image.width + x] +=
                    radiance * (1.0f / (f32)sampleCount);
            }
        }
    }
}

internal u32 AddDiffuseMaterial(MaterialSystem *materialSystem, vec3 albedo)
{
    Assert(materialSystem->count < ArrayCount(materialSystem->materials));
    u32 index = materialSystem->count++;

    materialSystem->materials[index].albedo = albedo;

    return index;
}

internal u32 AddEmissionMaterial(MaterialSystem *materialSystem, vec3 emission)
{
    Assert(materialSystem->count < ArrayCount(materialSystem->materials));
    u32 index = materialSystem->count++;

    materialSystem->materials[index].emission = emission;

    return index;
}

internal void AddMeshToScene(Scene *scene, Mesh mesh, u32 material)
{
    Assert(scene->meshCount < ArrayCount(scene->meshes));
    u32 index = scene->meshCount++;
    scene->meshes[index] = mesh;
}
