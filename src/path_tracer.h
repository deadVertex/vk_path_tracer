#pragma once

typedef __m256 simd_f32;

struct simd_vec3
{
    __m256 x;
    __m256 y;
    __m256 z;
};

struct RayIntersectTriangleResult
{
    f32 t;
    vec2 uv;
    vec3 normal;
};

struct RayIntersectSceneResult
{
    vec3 normal;
    vec3 position;
    vec2 textureCoordinates;
    f32 t;
    u32 materialId;
};

struct Sphere
{
    vec3 center;
    f32 radius;
    u32 materialId;
};

struct TextureSystem
{
    Texture texture;
};

struct Material
{
    vec3 albedo;
    vec3 emission;
};

struct MaterialSystem
{
    Material materials[32];
    u32 count;

    TextureSystem textureSystem;
};

struct Mesh
{
    vec3 *vertices;
    u32 *indices;
    u32 vertexCount;
    u32 indexCount;
};

struct Scene
{
    Mesh meshes[8];
    u32 meshCount;

    MaterialSystem materialSystem;
};

struct RayPacketIntersectSceneResult
{
    RayIntersectSceneResult entries[8];
};

struct LightPathVertex
{
    vec3 incomingDir;
    vec3 outgoingDir;
    vec3 normal;
    vec2 textureCoordinates;
    u32 materialId;
};

struct Warp
{
    vec3 rayOrigin[8];
    vec3 rayDirection[8];
    LightPathVertex path[8][3];
    u32 pathLength[8];
    vec3 radiance[8];
    u32 pixelX[8];
    u32 pixelY[8];
};

struct WorkingImage
{
    vec3 *pixels;
    u32 width;
    u32 height;
};

struct Tile
{
    u32 minX;
    u32 minY;
    u32 maxX;
    u32 maxY;
};
