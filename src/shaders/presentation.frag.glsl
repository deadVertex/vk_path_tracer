#version 450

layout(location = 0) out vec4 outputColor;

layout(binding = 0) uniform sampler2D u_ColorSampler2D;

layout(location=0) in vec2 fragTextureCoords;

vec3 ReinhardToneMapping(vec3 color)
{
    color *= 0.4; // exposure adjustment

    color = color / (vec3(1.0) + color);

    // Perfrom linear to sRGB conversion
    return pow(color, vec3(1.0 / 2.2));
}

void main()
{
    vec3 textureColor = texture(u_ColorSampler2D, fragTextureCoords).rgb;
    //vec3 tonemappedColor = ReinhardToneMapping(textureColor);
    outputColor = vec4(textureColor, 1);
}
