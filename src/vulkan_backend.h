#pragma once

//! Maximum limit for the number of GPUs queried on startup
#define MAX_SUPPORTED_GPUS 16

//! Maximum number of physical device queue families which will be queried
#define MAX_SUPPORTED_QUEUE_FAMILIES 64

//! Maximum limit on the number of extensions that can be requested when
// creating an instance
#define MAX_SUPPORTED_EXTENSIONS 16

//! Maximum number of images that can be present in a swapchain
#define MAX_SWAPCHAIN_IMAGES 2

//! Maxium number of vulkan descriptor sets that can be allocated
#define MAX_DESCRIPTOR_SETS 64

//! Helper macro for asserting that a Vulkan API call returned VK_SUCCESS
#define VK_CHECK(RESULT) Assert(RESULT == VK_SUCCESS)

#define MAX_PIPELINES 64
#define MAX_TEXTURES 64
#define MAX_RENDER_PASSES 4

#define MAX_UNIFORM_BUFFER_DESCRIPTORS 64
#define MAX_STORAGE_BUFFER_DESCRIPTORS 64

#define VERTEX_BUFFER_SIZE Kilobytes(64)
#define INDEX_BUFFER_SIZE Kilobytes(64)
#define MODEL_MATRIX_BUFFER_SIZE Kilobytes(1)
#define TEXTURE_UPLOAD_BUFFER_SIZE Megabytes(256)
#define MATERIAL_BUFFER_SIZE Megabytes(1)
