/* TODO:
- Or triangles?
*/
#include <cstdio>
#include <cmath>
#include <cfloat>

#include <immintrin.h> // For AVX2

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#ifdef PLATFORM_LINUX
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>
#endif

#include "platform.h"
#include "math_utils.h"
#include "math_lib.h"
#include "input.h"
#include "intrinsics.h"
#include "work_queue.h"
#include "asset_loader/asset_loader.h"
#include "path_tracer.h"

#define MAX_THREADS 8
#define TILE_WIDTH 256
#define SAMPLE_COUNT 1024
#define APPLICATION_MEMORY_LIMIT Megabytes(64)
#define CPU_PATH_TRACER_IMAGE_WIDTH 800
#define CPU_PATH_TRACER_IMAGE_HEIGHT 600

#include "path_tracer.cpp"

#include "vulkan_backend.h"
#include "vulkan_helpers.h"

#include "vulkan_backend.cpp"
#include "vulkan_helpers.cpp"

// #define STB_IMAGE_IMPLEMENTATION
// #include "stb_image.h"

internal u32 BuildRequestedExtensionsList(const char **requestedExtensions,
    u32 requestedCount, const char **extensions, u32 capacity)
{
    u32 glfwExtensionsCount = 0;
    const char **glfwExtensions =
        glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);

    u32 count = 0;
    for (u32 i = 0; i < requestedCount; ++i)
    {
        Assert(count < capacity);
        extensions[count++] = requestedExtensions[i];
    }

    for (u32 i = 0; i < glfwExtensionsCount; ++i)
    {
        Assert(count < capacity);
        extensions[count++] = glfwExtensions[i];
    }

    return count;
}

struct VulkanRenderer
{
    VkInstance instance;
    VkDevice device;
    VkSemaphore acquireSemaphore;
    VkSemaphore releaseSemaphore;
    VkCommandBuffer commandBuffer;
    VkCommandPool commandPool;
    VkQueue graphicsQueue;
    VkQueue presentQueue;
    VkSurfaceKHR surface;
    VkPhysicalDevice physicalDevice;

    SwapchainState swapchainState;

    BufferState textureUploadBuffer;
    ImageState cpuPathTracerImage;

    VkPipeline pipeline;
    VkPipelineLayout pipelineLayout;
    VkDescriptorSet descriptorSet;
};

struct ShaderByteCode
{
    u32 *vertexData;
    u32 *fragmentData;
    u32 vertexDataLength;
    u32 fragmentDataLength;
};

internal VulkanRenderer InitializeVulkanLayer(
    GLFWwindow *window, ShaderByteCode shaderByteCode)
{
    // TODO: Check that this layer is supported on our hardware
    const char *layers[] = {"VK_LAYER_KHRONOS_validation"};
    const char *extensions[MAX_SUPPORTED_EXTENSIONS] = {};

    const char *requestedExtensions[] = {VK_EXT_DEBUG_UTILS_EXTENSION_NAME};
    u32 extensionCount = BuildRequestedExtensionsList(requestedExtensions,
        ArrayCount(requestedExtensions), extensions, ArrayCount(extensions));

    VkInstance instance = CreateInstance(VK_API_VERSION_1_2, layers,
        ArrayCount(layers), extensions, extensionCount);
    VkPhysicalDevice physicalDevice = PickFirstPhysicalDevice(instance);

    VkSurfaceKHR surface = VK_NULL_HANDLE;
    VK_CHECK(glfwCreateWindowSurface(instance, window, NULL, &surface));

    u32 graphicsQueueFamilyIndex = FindGraphicsQueueFamily(physicalDevice);
    Assert(CheckSurfaceIsSupported(
        physicalDevice, surface, graphicsQueueFamilyIndex));
    // TODO: Find presentation queue family

    VkDevice device =
        CreateLogicalDevice(physicalDevice, graphicsQueueFamilyIndex);

    // TODO: Don't assume graphics and present queue are from the same family
    VkQueue graphicsQueue = GetQueue(device, graphicsQueueFamilyIndex, 0);
    VkQueue presentQueue = graphicsQueue;

    VkRenderPass renderPass = CreateRenderPass(
        device, VK_FORMAT_B8G8R8A8_UNORM, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

    VkSurfaceCapabilitiesKHR surfaceCapabilities = {
        VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR};
    VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
        physicalDevice, surface, &surfaceCapabilities));

    // Swap chain creation....
    u32 imageCount = 2;
    u32 imageWidth = surfaceCapabilities.currentExtent.width;
    u32 imageHeight = surfaceCapabilities.currentExtent.height;
    VkFormat imageFormat = VK_FORMAT_B8G8R8A8_UNORM;

    SwapchainState swapchainState =
        ConfigureSwapchain(device, surface, physicalDevice, renderPass,
            imageCount, imageFormat, imageWidth, imageHeight);

    VkDescriptorPool descriptorPool = CreateDescriptorPool(device);

    VkCommandPool commandPool =
        CreateCommandPool(device, graphicsQueueFamilyIndex);

    VkCommandBuffer commandBuffer = CreateCommandBuffer(device, commandPool);

    // Buffers (just need texture upload buffer I think for now)
    BufferState textureUploadBuffer = CreateBuffer(device, physicalDevice,
        TEXTURE_UPLOAD_BUFFER_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);

    // Pipeline stuff
    VkPipelineCache pipelineCache = CreatePipelineCache(device);

    // clang-format off
    VkDescriptorSetLayoutBinding bindings[] = {
        // binding, descriptorType, descriptorCount, stageFlags
        {0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, VK_SHADER_STAGE_FRAGMENT_BIT},
    };
    // clang-format on

    VkDescriptorSetLayout descriptorSetLayout =
        CreateDescriptorSetLayout(device, bindings, ArrayCount(bindings));

    VkPipelineLayout pipelineLayout =
        CreatePipelineLayout(device, descriptorSetLayout, 0,
            VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);

    VkShaderModule vertexShader = CreateShader(
        device, shaderByteCode.vertexData, shaderByteCode.vertexDataLength);
    VkShaderModule fragmentShader = CreateShader(
        device, shaderByteCode.fragmentData, shaderByteCode.fragmentDataLength);

    VkPipeline pipeline = CreatePipeline(device, vertexShader, fragmentShader,
        pipelineLayout, renderPass, pipelineCache);

    VkDescriptorSet descriptorSet =
        AllocateDescriptorSet(device, descriptorPool, descriptorSetLayout);

    VkSampler defaultSampler = CreateDefaultSampler(device);

    // Create image from CPU ray tracer
    VkFormat format = VK_FORMAT_R32G32B32A32_SFLOAT;
    ImageState cpuPathTracerImage =
        CreateImageState(device, physicalDevice, CPU_PATH_TRACER_IMAGE_WIDTH,
            CPU_PATH_TRACER_IMAGE_HEIGHT, VK_FORMAT_R32G32B32A32_SFLOAT,
            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_IMAGE_ASPECT_COLOR_BIT);

    TransitionImageLayout(device, cpuPathTracerImage.handle,
        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        commandPool, graphicsQueue);

    // Write to descriptor set
    {
        VkDescriptorImageInfo imageInfo = {};
        imageInfo.sampler = defaultSampler;
        imageInfo.imageView = cpuPathTracerImage.view;
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        VkWriteDescriptorSet descriptorWrites[1] = {};
        {
            descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptorWrites[0].dstSet = descriptorSet;
            descriptorWrites[0].dstBinding = 0;
            descriptorWrites[0].descriptorType =
                VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            descriptorWrites[0].descriptorCount = 1;
            descriptorWrites[0].pImageInfo = &imageInfo;
        }

        vkUpdateDescriptorSets(
            device, ArrayCount(descriptorWrites), descriptorWrites, 0, NULL);
    }

    // Create render pass
    VulkanRenderer result = {};
    result.instance = instance;
    result.device = device;
    result.commandBuffer = commandBuffer;
    result.commandPool = commandPool;
    result.swapchainState = swapchainState;
    result.acquireSemaphore = CreateSemaphore(device);
    result.releaseSemaphore = CreateSemaphore(device);
    result.graphicsQueue = graphicsQueue;
    result.presentQueue = presentQueue;
    result.pipeline = pipeline;
    result.pipelineLayout = pipelineLayout;
    result.descriptorSet = descriptorSet;
    result.surface = surface;
    result.physicalDevice = physicalDevice;
    result.textureUploadBuffer = textureUploadBuffer;
    result.cpuPathTracerImage = cpuPathTracerImage;

    return result;
}

internal void LogVkResult(VkResult result)
{
    if (result != VK_SUCCESS)
    {
        LogMessage("vkQueuePresentKHR failed");
        switch (result)
        {
        case VK_SUBOPTIMAL_KHR:
            LogMessage("VK_SUBOPTIMAL_KHR");
            break;

        case VK_ERROR_OUT_OF_HOST_MEMORY:
            LogMessage("VK_ERROR_OUT_OF_HOST_MEMORY");
            break;

        case VK_ERROR_OUT_OF_DEVICE_MEMORY:
            LogMessage("VK_ERROR_OUT_OF_DEVICE_MEMORY");
            break;

        case VK_ERROR_DEVICE_LOST:
            LogMessage("VK_ERROR_DEVICE_LOST");
            break;

        case VK_ERROR_OUT_OF_DATE_KHR:
            LogMessage("VK_ERROR_OUT_OF_DATE_KHR");
            break;

        case VK_ERROR_SURFACE_LOST_KHR:
            LogMessage("VK_ERROR_SURFACE_LOST_KHR");
            break;

        case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT:
            LogMessage("VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT");
            break;

        default:
            break;
        }
    }
}

internal void OnFramebufferResize(
    VulkanRenderer *renderer, u32 framebufferWidth, u32 framebufferHeight)
{
    // Make doubly sure no resources are still in use
    vkDeviceWaitIdle(renderer->device);

    // Save imageCount and imageFormat for use later
    u32 imageCount = renderer->swapchainState.imageCount;
    VkFormat imageFormat = renderer->swapchainState.imageFormat;
    VkRenderPass renderPass = renderer->swapchainState.renderPass;

    // Destroy current swapchain
    UnconfigureSwapchain(renderer->swapchainState, renderer->device);

    // Create new one with the new dimensions
    renderer->swapchainState = ConfigureSwapchain(renderer->device,
        renderer->surface, renderer->physicalDevice, renderPass, imageCount,
        imageFormat, framebufferWidth, framebufferHeight);
}

internal void UploadCpuPathTracerImageToGpu(VulkanRenderer *renderer)
{
    u32 width = CPU_PATH_TRACER_IMAGE_WIDTH;
    u32 height = CPU_PATH_TRACER_IMAGE_HEIGHT;

    TransitionImageLayout(renderer->device, renderer->cpuPathTracerImage.handle,
        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        renderer->commandPool, renderer->graphicsQueue);

    CopyBufferToImage(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->textureUploadBuffer.handle,
        renderer->cpuPathTracerImage.handle, width, height, 1, 0);

    TransitionImageLayout(renderer->device, renderer->cpuPathTracerImage.handle,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, renderer->commandPool,
        renderer->graphicsQueue);
}

internal void RenderFrame(VulkanRenderer *renderer)
{
    // Begin Frame
    u32 imageIndex = 0;
    VK_CHECK(
        vkAcquireNextImageKHR(renderer->device, renderer->swapchainState.handle,
            0, renderer->acquireSemaphore, VK_NULL_HANDLE, &imageIndex));

    VK_CHECK(vkResetCommandPool(renderer->device, renderer->commandPool, 0));

    VkCommandBufferBeginInfo beginInfo = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    VK_CHECK(vkBeginCommandBuffer(renderer->commandBuffer, &beginInfo));

    u32 framebufferWidth = renderer->swapchainState.imageWidth;
    u32 framebufferHeight = renderer->swapchainState.imageHeight;

    // Begin presentation render pass
    {
        VkClearValue clearValues[2] = {};
        clearValues[0].color = {0.08f, 0.01f, 0.04f, 1.0f};
        clearValues[1].depthStencil = {1.0f, 0};

        VkRenderPassBeginInfo renderPassBegin = {
            VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
        renderPassBegin.renderPass = renderer->swapchainState.renderPass;
        renderPassBegin.framebuffer =
            renderer->swapchainState.framebuffers[imageIndex];
        renderPassBegin.renderArea.extent.width = framebufferWidth;
        renderPassBegin.renderArea.extent.height = framebufferHeight;
        renderPassBegin.clearValueCount = ArrayCount(clearValues);
        renderPassBegin.pClearValues = clearValues;
        vkCmdBeginRenderPass(renderer->commandBuffer, &renderPassBegin,
            VK_SUBPASS_CONTENTS_INLINE);

        // Set dynamic pipeline state
        VkViewport viewport = {
            0, 0, (f32)framebufferWidth, (f32)framebufferHeight, 0.0f, 1.0f};
        VkRect2D scissor = {0, 0, framebufferWidth, framebufferHeight};
        vkCmdSetViewport(renderer->commandBuffer, 0, 1, &viewport);
        vkCmdSetScissor(renderer->commandBuffer, 0, 1, &scissor);
    }

    vkCmdBindDescriptorSets(renderer->commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS, renderer->pipelineLayout, 0, 1,
        &renderer->descriptorSet, 0, NULL);

    vkCmdBindPipeline(renderer->commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
        renderer->pipeline);

    vkCmdDraw(renderer->commandBuffer, 6, 1, 0, 0);

    vkCmdEndRenderPass(renderer->commandBuffer);

    // End frame
    VK_CHECK(vkEndCommandBuffer(renderer->commandBuffer));

    // Submit command buffer to GPU
    VkPipelineStageFlags submitStageMask =
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo = {VK_STRUCTURE_TYPE_SUBMIT_INFO};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &renderer->acquireSemaphore;
    submitInfo.pWaitDstStageMask = &submitStageMask;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &renderer->commandBuffer;
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &renderer->releaseSemaphore;
    VK_CHECK(
        vkQueueSubmit(renderer->graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));

    VkPresentInfoKHR presentInfo = {VK_STRUCTURE_TYPE_PRESENT_INFO_KHR};
    presentInfo.swapchainCount = 1;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &renderer->releaseSemaphore;
    presentInfo.pSwapchains = &renderer->swapchainState.handle;
    presentInfo.pImageIndices = &imageIndex;
    VkResult presentResult =
        vkQueuePresentKHR(renderer->presentQueue, &presentInfo);
    if (presentResult == VK_ERROR_OUT_OF_DATE_KHR ||
        presentResult == VK_SUBOPTIMAL_KHR)
    {
        OnFramebufferResize(renderer, framebufferWidth, framebufferHeight);
    }
    else
    {
        LogVkResult(presentResult);
    }

    // FIXME: Remove this to allow CPU and GPU to run in parallel
    VK_CHECK(vkDeviceWaitIdle(renderer->device));
}

// FIXME: Move this out of here!!!!
struct FileData
{
    u32 length;
    void *contents;
};

// FIXME: This isn't actually mapping the file into memory!!!!!!
#pragma warning(push)
#pragma warning(disable : 4996)
FileData MapFileIntoMemory(const char *path)
{
    FileData result = {};
    FILE *file = fopen(path, "rb");
    if (file != NULL)
    {
        fseek(file, 0, SEEK_END);
        u32 length = (u32)ftell(file);
        fseek(file, 0, SEEK_SET);

        void *contents = malloc(length);
        if (contents != NULL)
        {
            if (fread(contents, length, 1, file) == 1)
            {
                // Success
                result.contents = contents;
                result.length = length;
            }
            else
            {
                LogMessage(
                    "Failed to read %u bytes from file %s\n", length, path);
                free(contents); // Free memory as we won't return it if the read
                                // fails
            }
        }
        else
        {
            LogMessage(
                "Failed to allocate %u bytes for file %s\n", length, path);
        }

        fclose(file);
    }
    else
    {
        LogMessage("Failed to open file %s\n", path);
    }
    return result;
}

void FreeFileDataFromMemory(FileData fileData)
{
    Assert(fileData.contents != NULL);
    free(fileData.contents);
}
#pragma warning(pop)

internal ShaderByteCode LoadShaderByteCode(
    const char *vertexShaderPath, const char *fragmentShaderPath)
{
    FileData vertexShaderFile = MapFileIntoMemory(vertexShaderPath);

    // Validate the file input
    Assert(vertexShaderFile.contents != NULL);
    Assert(vertexShaderFile.length % 4 == 0);

    FileData fragmentShaderFile = MapFileIntoMemory(fragmentShaderPath);

    // Validate the file input
    Assert(fragmentShaderFile.contents != NULL);
    Assert(fragmentShaderFile.length % 4 == 0);

    ShaderByteCode result = {};
    result.vertexData = (u32 *)vertexShaderFile.contents;
    result.vertexDataLength = vertexShaderFile.length;
    result.fragmentData = (u32 *)fragmentShaderFile.contents;
    result.fragmentDataLength = fragmentShaderFile.length;

    // FIXME: So when do we free this...
    // freeFileDataFromMemory(vertexShaderFile);
    // freeFileDataFromMemory(fragmentShaderFile);
    return result;
}

internal DebugLogMessage(LogMessage_)
{
    char buffer[256];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);
#ifdef PLATFORM_WINDOWS
    OutputDebugString(buffer);
    OutputDebugString("\n");
#endif
    puts(buffer);
}

#define KEY_HELPER(NAME)                                                       \
    case GLFW_KEY_##NAME:                                                      \
        return KEY_##NAME;
internal i32 ConvertKey(int key)
{
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT)
    {
        return key;
    }
    switch (key)
    {
        KEY_HELPER(BACKSPACE);
        KEY_HELPER(TAB);
        KEY_HELPER(INSERT);
        KEY_HELPER(HOME);
        KEY_HELPER(PAGE_UP);
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE:
        return KEY_DELETE;
        KEY_HELPER(END);
        KEY_HELPER(PAGE_DOWN);
        KEY_HELPER(ENTER);

        KEY_HELPER(LEFT_SHIFT);
    case GLFW_KEY_LEFT_CONTROL:
        return KEY_LEFT_CTRL;
        KEY_HELPER(LEFT_ALT);
        KEY_HELPER(RIGHT_SHIFT);
    case GLFW_KEY_RIGHT_CONTROL:
        return KEY_RIGHT_CTRL;
        KEY_HELPER(RIGHT_ALT);

        KEY_HELPER(LEFT);
        KEY_HELPER(RIGHT);
        KEY_HELPER(UP);
        KEY_HELPER(DOWN);

        KEY_HELPER(ESCAPE);

        KEY_HELPER(F1);
        KEY_HELPER(F2);
        KEY_HELPER(F3);
        KEY_HELPER(F4);
        KEY_HELPER(F5);
        KEY_HELPER(F6);
        KEY_HELPER(F7);
        KEY_HELPER(F8);
        KEY_HELPER(F9);
        KEY_HELPER(F10);
        KEY_HELPER(F11);
        KEY_HELPER(F12);
    case GLFW_KEY_KP_0:
        return KEY_NUM0;
    case GLFW_KEY_KP_1:
        return KEY_NUM1;
    case GLFW_KEY_KP_2:
        return KEY_NUM2;
    case GLFW_KEY_KP_3:
        return KEY_NUM3;
    case GLFW_KEY_KP_4:
        return KEY_NUM4;
    case GLFW_KEY_KP_5:
        return KEY_NUM5;
    case GLFW_KEY_KP_6:
        return KEY_NUM6;
    case GLFW_KEY_KP_7:
        return KEY_NUM7;
    case GLFW_KEY_KP_8:
        return KEY_NUM8;
    case GLFW_KEY_KP_9:
        return KEY_NUM9;
    case GLFW_KEY_KP_DECIMAL:
        return KEY_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return KEY_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return KEY_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return KEY_NUM_MINUS;
    case GLFW_KEY_KP_ADD:
        return KEY_NUM_PLUS;
    case GLFW_KEY_KP_ENTER:
        return KEY_NUM_ENTER;
    }
    return KEY_UNKNOWN;
}

void KeyCallback(
    GLFWwindow *window, int glfwKey, int scancode, int action, int mods)
{
    GameInput *input = (GameInput *)glfwGetWindowUserPointer(window);

    i32 key = ConvertKey(glfwKey);
    if (key != KEY_UNKNOWN)
    {
        Assert(key < MAX_KEYS);
        if (action == GLFW_PRESS)
        {
            input->buttonStates[key].isDown = true;
        }
        else if (action == GLFW_RELEASE)
        {
            input->buttonStates[key].isDown = false;
        }
        // else: ignore key repeat messages
    }
}

#if defined(PLATFORM_LINUX)
internal void *AllocateMemory(u64 numBytes, u64 baseAddress = 0)
{
    void *result = mmap((void *)baseAddress, numBytes, PROT_READ | PROT_WRITE,
        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    Assert(result != MAP_FAILED);

    return result;
}

internal void FreeMemory(void *p) { munmap(p, 0); }
#endif

struct Task
{
    Scene *scene;
    WorkingImage image;
    Tile tile;
};

internal void WorkerThread(WorkQueue *queue)
{
    while (1)
    {
        // If not empty
        if (queue->head != queue->tail)
        {
            RandomNumberGenerator rng = {};
            rng.state = 0xDEADBEEF; // TODO: Probably shouldn't give each thread
                                    // same seed

            // Work to do
            Task *task = (Task *)WorkQueuePop(queue, sizeof(Task));
            PathTraceTile(task->scene, &rng, task->image, task->tile);
        }
        else
        {
            // FIXME: Use a semaphore for signalling
#ifdef PLATFORM_WINDOWS
            Sleep(10);
#elif defined(PLATFORM_LINUX)
            usleep(1000);
#endif
        }
    }
}

#ifdef PLATFORM_WINDOWS
internal DWORD WinWorkerThreadProc(LPVOID lpParam)
{
    WorkerThread((WorkQueue *)lpParam);
    return 0;
}
#elif defined(PLATFORM_LINUX)
internal void *LinuxWorkerThreadProc(void *arg)
{
    WorkerThread((WorkQueue *)arg);
    return NULL;
}
#endif

struct ThreadMetaData
{
#ifdef PLATFORM_WINDOWS
    HANDLE handle;
    DWORD id;
#elif defined(PLATFORM_LINUX)
    pthread_t handle;
#endif
};

struct ThreadPool
{
    ThreadMetaData threads[MAX_THREADS];
};

internal ThreadPool CreateThreadPool(WorkQueue *queue)
{
    ThreadPool pool = {};

#ifdef PLATFORM_WINDOWS
    for (u32 threadIndex = 0; threadIndex < MAX_THREADS; ++threadIndex)
    {
        ThreadMetaData metaData = {};
        metaData.handle =
            CreateThread(NULL, 0, WinWorkerThreadProc, queue, 0, &metaData.id);
        Assert(metaData.handle != INVALID_HANDLE_VALUE);
        LogMessage("ThreadId is %u", metaData.id);

        pool.threads[threadIndex] = metaData;
    }
#elif defined(PLATFORM_LINUX)
    for (u32 threadIndex = 0; threadIndex < MAX_THREADS; ++threadIndex)
    {
        ThreadMetaData metaData = {};
        int ret = pthread_create(
            &metaData.handle, NULL, LinuxWorkerThreadProc, queue);
        Assert(ret == 0);
        LogMessage("Thread handle is %u", metaData.handle);

        pool.threads[threadIndex] = metaData;
    }
#endif
    return pool;
}

internal Mesh CreatePlaneMesh(MemoryArena *arena)
{
    vec3 planeVertices[4] = {{Vec3(-0.5, -0.5, 0)}, {Vec3(0.5, -0.5, 0)},
        {Vec3(0.5, 0.5, 0)}, {Vec3(-0.5, 0.5, 0)}};

    u32 planeIndices[6] = {0, 1, 2, 2, 3, 0};

    Mesh meshData = {};
    meshData.vertices = (vec3 *)AllocateBytes(arena, sizeof(planeVertices));
    meshData.vertexCount = 4;
    CopyMemory(meshData.vertices, planeVertices, sizeof(planeVertices));

    meshData.indices = (u32 *)AllocateBytes(arena, sizeof(planeIndices));
    meshData.indexCount = 6;
    CopyMemory(meshData.indices, planeIndices, sizeof(planeIndices));

    return meshData;
}

int main(int argc, char **argv)
{
    LogMessage = &LogMessage_;

    LogMessage("Hello vk_path_tracer!\n");

    if (!glfwInit())
    {
        LogMessage("Failed to initialize GLFW\n");
        return -1;
    }

    u32 framebufferWidth = CPU_PATH_TRACER_IMAGE_WIDTH;
    u32 framebufferHeight = CPU_PATH_TRACER_IMAGE_HEIGHT;
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    GLFWwindow *window = glfwCreateWindow(
        framebufferWidth, framebufferHeight, "vk_path_tracer", NULL, NULL);

    if (window == NULL)
    {
        LogMessage("Failed to create window\n");
        glfwTerminate();
        return -1;
    }

    GameInput input = {};
    glfwSetWindowUserPointer(window, &input);
    glfwSetKeyCallback(window, KeyCallback);

    // Create memory arenas
    u32 applicationMemorySize = APPLICATION_MEMORY_LIMIT;
    MemoryArena applicationMemoryArena = {};
    InitializeMemoryArena(&applicationMemoryArena,
        AllocateMemory(applicationMemorySize), applicationMemorySize);

    u32 workQueueArenaSize = Kilobytes(32);
    MemoryArena workQueueArena =
        SubAllocateArena(&applicationMemoryArena, workQueueArenaSize);

    WorkQueue workQueue = CreateWorkQueue(&workQueueArena, sizeof(Task), 256);
    ThreadPool threadPool = CreateThreadPool(&workQueue);

    u32 meshDataArenaSize = Megabytes(16);
    MemoryArena meshDataArena =
        SubAllocateArena(&applicationMemoryArena, meshDataArenaSize);

    ShaderByteCode shaderByteCode = LoadShaderByteCode(
        "shaders/presentation.vert.spv", "shaders/presentation.frag.spv");
    VulkanRenderer vulkanRenderer =
        InitializeVulkanLayer(window, shaderByteCode);

    Scene scene = {};
    MaterialSystem *materialSystem = &scene.materialSystem;
    u32 skyMaterial = AddEmissionMaterial(
        materialSystem, Vec3(0.39, 0.58, 0.93)); // Sky color
    u32 whiteDiffuseMaterial = AddDiffuseMaterial(materialSystem, Vec3(1));
    u32 greyDiffuseMaterial = AddDiffuseMaterial(materialSystem, Vec3(0.2));
    u32 pinkDiffuseMaterial = AddDiffuseMaterial(materialSystem, Vec3(1, 0, 1));
    u32 orangeLightMaterial =
        AddEmissionMaterial(materialSystem, Vec3(1.6, 1.4, 1.0));

    Texture rockAlbedoTexture = {};
    b32 textureLoaded = LoadTexture(
        "../assets/textures/Rock050_2K_Color.png", &rockAlbedoTexture);
    Assert(textureLoaded);
    materialSystem->textureSystem.texture = rockAlbedoTexture;

    Mesh testMesh = CreatePlaneMesh(&meshDataArena);

    AddMeshToScene(&scene, testMesh, whiteDiffuseMaterial);

    RandomNumberGenerator rng = {};
    rng.state = 0xDEADBEEF;

    u32 imageWidth = CPU_PATH_TRACER_IMAGE_WIDTH;
    u32 imageHeight = CPU_PATH_TRACER_IMAGE_HEIGHT;
    vec4 *pixels = (vec4 *)vulkanRenderer.textureUploadBuffer.data;

    WorkingImage image = {};
    image.width = CPU_PATH_TRACER_IMAGE_WIDTH;
    image.height = CPU_PATH_TRACER_IMAGE_HEIGHT;
    image.pixels = (vec3 *)calloc(image.width * image.height, sizeof(vec3));

    while (!glfwWindowShouldClose(window))
    {
        f64 frameStart = glfwGetTime();
        InputBeginFrame(&input);
        glfwPollEvents();

        // Check for framebuffer resize
        // NOTE: Workaround what seems like a GLFW bug where
        // glfwGetFramebufferSize differs from the VkSurfaceCapabilities
        VkSurfaceCapabilitiesKHR surfaceCapabilities = {
            VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR};
        VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
            vulkanRenderer.physicalDevice, vulkanRenderer.surface,
            &surfaceCapabilities));

        u32 newFramebufferWidth = surfaceCapabilities.currentExtent.width;
        u32 newFramebufferHeight = surfaceCapabilities.currentExtent.height;

        if (newFramebufferWidth != framebufferWidth ||
            newFramebufferHeight != framebufferHeight)
        {
            LogMessage("Framebuffer resized to %d x %d\n", newFramebufferWidth,
                newFramebufferHeight);
            OnFramebufferResize(
                &vulkanRenderer, newFramebufferWidth, newFramebufferHeight);
            framebufferWidth = newFramebufferWidth;
            framebufferHeight = newFramebufferHeight;
        }

        if (WasPressed(input.buttonStates[KEY_SPACE]))
        {
            // Zero image
            for (u32 y = 0; y < image.height; y++)
            {
                for (u32 x = 0; x < image.width; x++)
                {
                    image.pixels[y * image.width + x] = Vec3(0);
                }
            }

            u32 tileWidth = TILE_WIDTH;
            u32 tileCountX = (image.width / tileWidth) + 1;
            u32 tileCountY = (image.height / tileWidth) + 1;

            for (u32 y = 0; y < tileCountY; y++)
            {
                for (u32 x = 0; x < tileCountX; x++)
                {
                    Tile tile = {};
                    tile.minX = MinU32(x * tileWidth, image.width);
                    tile.minY = MinU32(y * tileWidth, image.height);
                    tile.maxX = MinU32(tile.minX + tileWidth, image.width);
                    tile.maxY = MinU32(tile.minY + tileWidth, image.height);

                    Task task = {};
                    task.scene = &scene;
                    task.image = image;
                    task.tile = tile;
                    WorkQueuePush(&workQueue, &task, sizeof(task));
                }
            }
        }

        // Write to vulkan image to display
        for (u32 y = 0; y < image.height; y++)
        {
            for (u32 x = 0; x < image.width; x++)
            {
                vec3 radiance = image.pixels[y * image.width + x];
                pixels[y * image.width + x] = Vec4(radiance, 1);
            }
        }

        UploadCpuPathTracerImageToGpu(&vulkanRenderer);
        RenderFrame(&vulkanRenderer);

        f64 frameEnd = glfwGetTime();
        LogMessage("Frame time: %g ms", (frameEnd - frameStart) * 1000.0);
    }

    free(image.pixels);
    glfwTerminate();
    return 0;
}
